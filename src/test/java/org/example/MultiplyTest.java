package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiplyTest {

    @Test
    public void testMultiply() {
        assertEquals(6, Multiply.multiply(2,3));
        assertEquals(24, Multiply.multiply(6, 4));
    }
}
