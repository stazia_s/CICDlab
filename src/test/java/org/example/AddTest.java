package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AddTest {
    @Test
    public void testAdd() {
        assertEquals(5, Add.add(2,3));
        assertEquals(10, Add.add(6, 4));
    }
}
